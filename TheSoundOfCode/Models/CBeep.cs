﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSoundOfCode.Models
{
    public class CBeep
    {
        public uint Frecuency { get; set; }
        public uint Duration { get; set; }
    }
}
