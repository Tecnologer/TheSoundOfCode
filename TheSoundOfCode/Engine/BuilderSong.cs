﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using TheSoundOfCode.Sources;

namespace TheSoundOfCode.Engine
{
    public class BuilderSong
    {
        [DllImport("Kernel32.dll")]
        public static extern bool Beep(UInt32 frequency, UInt32 duration);

        private static object syncRoot = new object();
        private static BuilderSong instance;
        private List<string> currentSong;
        private int algo = 0;
        public static BuilderSong Instance
        {
            get
            {
                lock (syncRoot)
                    return instance ?? (instance = new BuilderSong());
            }
        }

        public BuilderSong()
        {
            currentSong = new List<string>();
        }
        public void AddKey(int vkCode)
        {
            var key = (Keys)vkCode;
            var sound = SoundSource.Instance.GetSound(key);
            if (sound == null) return;

            currentSong.Add(sound);
            if(currentSong.Count == 1)
                System.Diagnostics.Debug.WriteLine("=============== Start Keys added ===============");
            System.Diagnostics.Debug.Write(key);
        }

        internal void CreateSong()
        {
            System.Diagnostics.Debug.WriteLine("\n=============== Eds Keys added ===============");
            System.Diagnostics.Debug.WriteLine("Key F8 pressed.");
            var worker = new BackgroundWorker();
            worker.DoWork += BuildSong;
            var copyOfSounds = new List<string>(currentSong);
            worker.RunWorkerAsync(copyOfSounds);
            currentSong.Clear();
        }

        private void BuildSong(object sender, DoWorkEventArgs e)
        {
            var sounds = e.Argument as List<string>;
            if (sounds.Count == 0) return;

            var song = new List<byte>();

            foreach (string sound in sounds)
            {
                song.AddRange(File.ReadAllBytes(sound));
            }

            WriteSong(song.ToArray(), 0);
        }

        private void WriteSong(byte[] song, int retry = 0)
        {
            var songName = string.Format("TheSoundOfCode{0}.mp3", retry > 0 ? "_" + retry : "");
            try
            {
                var songPath = Path.Combine(Settings.ProjectDirectory, songName);
                File.WriteAllBytes(songPath, song);
            }
            catch
            {
                if(retry < 101)
                    WriteSong(song, retry + 1);
            }

            System.Diagnostics.Debug.WriteLine(string.Format("THE SOUND OF YOUR CODE WAS CREATED. [{0}]", songName));
        }
        public void PlaySoundAsync(int vkCode)
        {
            var worker = new BackgroundWorker();
            worker.DoWork += PlaySound;
            worker.RunWorkerAsync(vkCode);
        }
        private void PlaySound(object sender, DoWorkEventArgs e)
        {
            var key = (Keys)((int)e.Argument);
            if (!SoundSource.Instance.Beeps.ContainsKey(key)) return;
            var beep = SoundSource.Instance.Beeps[key];

            Beep(beep.Frecuency, beep.Duration);
        }
    }
}
