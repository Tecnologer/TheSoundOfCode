﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using TheSoundOfCode.Models;

namespace TheSoundOfCode.Sources
{
    public class SoundSource
    {
        private static object syncRoot = new object();
        private static SoundSource instance;
        private Dictionary<Keys, string> sounds;
        private Dictionary<Keys, CBeep> beeps;

        public static SoundSource Instance
        {
            get
            {
                lock (syncRoot)
                    return instance ?? (instance = new SoundSource());
            }
        }

        public Dictionary<Keys, string> Sounds
        {
            get
            {
                return sounds;
            }
        }

        public Dictionary<Keys, CBeep> Beeps
        {
            get
            {
                return beeps;
            }
        }

        internal string GetSound(Keys key)
        {
            if (sounds.ContainsKey(key))
                return sounds[key];

            return null;
        }

        public SoundSource()
        {
            sounds = new Dictionary<Keys, string>();
            beeps = new Dictionary<Keys, CBeep>();
            PopulateSounds();
        }

        private void PopulateSounds()
        {
            var directory = Path.Combine(Settings.ProjectDirectory, "Sounds");
            var files = Directory.GetFiles(directory);

            var i = 0;
            foreach(Keys key in Enum.GetValues(typeof(Keys)))
            {
                if (sounds.ContainsKey(key)) continue;
                sounds.Add(key, files[i]);
                beeps.Add(key, new CBeep()
                {
                    Frecuency = GetFrecuency(key, i),
                    Duration = 60
                });

                i++;
                if (i >= files.Length)
                    i = 0;
            }
        }

        private uint GetFrecuency(Keys key, int modificator)
        {
            var vkCode = (int)key;
            double factor = Math.Pow(2.0, 1.0 / 1200.0);
            return ((uint)(100 * Math.Pow(factor, vkCode + modificator * 1200.0)));
        }
    }
}
